#include <iostream>
#include <random>

int maxNumber = 101;
int minNumber = 0;
int guess;
std::string input;

int main()
{

    std::cout << "Think of a number between 0 and " << maxNumber - 1 << "\n";

    do
    {   // Code from https://learn.microsoft.com/en-us/cpp/standard-library/random?view=msvc-170
        std::random_device rd;   // Non-deterministic generator
        std::mt19937 gen(rd());  // To seed mersenne twister.
        std::uniform_int_distribution<> dist(minNumber,maxNumber); // Distribute results between 1 and 6 inclusive.
        
        // Make a randmm guess bewteen a min and max value
        guess = dist(gen);

        std::cout << "Is the number " << guess << "?\n heigher, lower, yes\n"; 
        std::cin >> input;

        // If the guess was to low
        if (input == "heigher")
        {
            // Set the minimum we can guess next time to this value
            minNumber = guess;
        }
        // If the guess was to heigh
        else if (input == "lower")
        {
            // Set the maxiimum we can guess next time to this value
            maxNumber = guess;
        }

    } while (input != "yes"); // Exit i correct guess

    return 0;
}