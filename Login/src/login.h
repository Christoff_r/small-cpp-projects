#include<iostream>
#include <utility>
#include <string>
#include <vector>

class login
{
private:
    std::vector<std::pair<std::string, std::string>> users;

public:
    login();
    ~login();

    void createUser(std::string username, std::string password);
    void listUsersAndPasswords();
    bool logInUser(std::string username, std::string password, bool isLogedIn);
    bool logOutUser();
    void quit();
    bool deleteUser(std::string username, std::string password, bool isLogedIn);

    // Save
    template<class B>
    void serialize(B& buf) const
    {
        buf << users;
    }
};

