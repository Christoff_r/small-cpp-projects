#include <fstream>
#include "login.h"
#include "../hps/src/hps.h"

login::login()
{
    auto in_stream = std::ifstream("../saves/users.txt", std::ios::binary);
    users = hps::from_stream<std::vector<std::pair<std::string, std::string>>>(in_stream);
    in_stream.close();
}

login::~login()
{
}

void login::createUser(std::string username, std::string password)
{

    for (size_t i = 0; i < users.size(); i++)
    {
        if (users[i].first == username)
        {
            std::cout << users[i].first << " is taking\n";
            return;
        }
    }

    users.push_back(std::make_pair(username, password));
    std::cout << "User created\n";
}

void login::listUsersAndPasswords()
{
    for (size_t i = 0; i < users.size(); i++)
    {
        std::cout << users[i].first << " , " << users[i].second << std::endl;
    }
}

bool login::logInUser(std::string username, std::string password, bool isLogedIn)
{
    if (isLogedIn == false)
    {
        for (size_t i = 0; i < users.size(); i++)
        {
            if (users[i].first == username && users[i].second == password)
            {
                std::cout << users[i].first << " loged in!\n";
                return true;
            }

            std::cout << "Username or password wrong or do not exist!\n";
            return false;
        }
    }
    else
    {
        std::cout << "User allready loged in!\n";
        return true;
    }
    std::cout << "User do not exist!\n";
    return false;
}

bool login::logOutUser()
{
    std::cout << "You Loged Out!\n";
    return false;
}

void login::quit()
{
    auto out_stream = std::ofstream("../saves/users.txt", std::ios::binary);
    hps::to_stream(users, out_stream);
    out_stream.close(); 
}

bool login::deleteUser(std::string username, std::string password, bool isLogedIn)
{
    for (size_t i = 0; i < users.size(); i++)
    {
        if (users[i].first == username && users[i].second == password)
        {
            std::cout << "Deleting " << username;
            users.erase(users.begin() + i);
            logOutUser();
        }
        return isLogedIn;
    }

}