#include <iostream>
#include <string>
#include "login.h"

int main()
{
    std::string username;
    std::string password;
    char command;
    bool isLogedIn = false;
    login *loginManeger = new login();

    while (1)
    {
        std::cout << "1: Create new user, 2: List User infomation, 3: Log in, 4. Log out, (d)elete user Or (q)uit\n";
        std::cin >> command;

        switch (command)
        {
        case '1':
            std::cout << "Enter username: ";
            std::cin >> username;
            std::cout << "Enter password: ";
            std::cin >> password;
            loginManeger->createUser(username, password);
            break;
        case '2':
            loginManeger->listUsersAndPasswords();
            break;
        case '3':
            std::cout << "Enter username: ";
            std::cin >> username;
            std::cout << "Enter password: ";
            std::cin >> password;
            isLogedIn = loginManeger->logInUser(username, password, isLogedIn);
            break;
        case '4':
            loginManeger->logOutUser();
            break;
        case 'd':
            std::cout << "Enter username: ";
            std::cin >> username;
            std::cout << "Enter password: ";
            std::cin >> password;
            loginManeger->deleteUser(username, password, isLogedIn);
            break;
        case 'q':
            loginManeger->quit();
            return 0;
            break;
        default:
            break;
        }
    }

    return 0;
}
